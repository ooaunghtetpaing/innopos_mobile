import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent  implements OnInit {

  users: any;

  constructor(
    private http: HttpService
  ) { }

  async ngOnInit() {
    this.http.getRequest("http://localhost:8000/api/users").subscribe(
      (result: any) => {
        console.log(result);
        this.users = result.data;
      },
    );
    // response.unsubscribe();
  }

}
