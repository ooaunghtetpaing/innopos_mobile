import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * HTTP REQUEST ERROR HANDLER FOR HTTP STATUS [500,400,403]
   * @param error 
   * @returns 
   */
  private httpErrorHandler = (error: any) => {
    if(error.status === 500 || error.status === 400 || error.status === 403) {
      return of({
        status: error.status,
        message: error.error.message
      });
    }
    return of(error);
  }

  /**
   * HTTP GET METHOD
   * @param path 
   * @param params 
   * @returns 
   */
  getRequest = (path: string, params?: any): Observable<any> => {
    return this.http.get(path, params).pipe(
      map(result => result),
      catchError(error => this.httpErrorHandler(error))
    );
  }

  /**
   * HTTP POST METHOD
   * @param path 
   * @param body 
   * @returns 
   */
  postRequest = (path: string, body: any): Observable<any> => {
    return this.http.post(path, body).pipe(
      map(result => result),
      catchError(error => this.httpErrorHandler(error))
    );
  }

  /**
   * HTTP PUT METHOD
   * @param path 
   * @param body 
   * @returns 
   */
  putReqest = (path: string, body: any): Observable<any> => {
    return this.http.put(path, body).pipe(
      map(result => result),
      catchError(error => this.httpErrorHandler(error))
    );
  }

  /**
   * HTTP DELETE METHOD
   * @param path 
   * @param params 
   * @returns 
   */
  delRequest = (path: string, params?: any): Observable<any> => {
    return this.http.delete(path, params).pipe(
      map(result => result),
      catchError(error => this.httpErrorHandler(error))
    );
  }
}
